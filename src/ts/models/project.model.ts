import { User } from "./user.model"

export interface Project {
    id: number
    name: string
    description: string
    labels: string[]
    owner: number | User
    sharedWith: number[] | User[]
    modifiedAt: string
}