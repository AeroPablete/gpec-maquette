import * as Handlebars from 'handlebars';

import { showSpinner } from './helpers'


/**
 * 
 * @param req 
 */
export const uploadJobDescription = (req) => {
    if (req.verb == 'get') {
        const projectId = req.params.projectId

        // Set Breadcrumbs
        const breadcrumbs = `
            <div class="breadcrumbs__list">
                <div class="breadcrumbs__item--link">
                    <a href="/#/projects">Projects</a>
                </div>

                <div class="breadcrumbs__sep">
                    &#10095;
                </div>

                <div class="breadcrumbs__item breadcrumbs__item--link">
                    <a href="/#/project?id=${projectId}">${projectId}</a>
                </div>

                <div class="breadcrumbs__sep">
                    &#10095;
                </div>

                <div class="breadcrumbs__item breadcrumbs__item--current">
                    Upload Job Description
                </div>
            </div>
        `
        $('#breadcrumbs').html(breadcrumbs)
        showSpinner()

        setTimeout(() => {
            const template = Handlebars.compile($('#jobdesc-upload-tmpl').html())
            const context = {}
            $('#main').html(template(context))
        }, 1000)
    }   
}

/**
 * 
 * @param req 
 */
export const displayJobDescription = (req) => {
    showSpinner()

    setTimeout(() => {
        const template = Handlebars.compile($('#query-filters-tmpl').html())
        const context = {}
        $('#main').html(template(context))
    }, 1000)
}

/**
 * 
 * @param req 
 */
export const displayCandidateFilters = (req) => {
    showSpinner()

    setTimeout(() => {
        const template = Handlebars.compile($('#query-filters-tmpl').html())
        const context = {
            showQueryFilters: true
        }
        $('#main').html(template(context))
    }, 1000)
}