import * as Handlebars from 'handlebars';
import { showSpinner } from './helpers';

export const showFaqPage = (req: any) => {
    // Set Breadcrumbs
    const breadcrumbs = `
        <div class="breadcrumbs__list">
            <div class="breadcrumbs__item">
                FAQ
            </div>
        </div>
    `
    $('#breadcrumbs').html(breadcrumbs)

    showSpinner()

    setTimeout(() => {
        const template = Handlebars.compile($('#faq-tmpl').html())
        const context = {}
        $('#main').html(template(context))
    }, 1000)
}