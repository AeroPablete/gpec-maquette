import * as Handlebars from 'handlebars';

/**
 * 
 */
export const showSpinner = () => {
    const template = Handlebars.compile($('#spinner-tmpl').html())
    $('#main').html(template({}))
}