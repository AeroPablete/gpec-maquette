import { DateTime } from "luxon";
import * as Handlebars from 'handlebars';

import { showSpinner } from './helpers'

import { Project, User } from "../models";
import * as projectData from '../models/data/project.data.json';
import * as userData from '../models/data/user.data.json';

/**
 * 
 * @param _ 
 */
export const getAllProjects = (_: any) => {
    const projects: Project[] = projectData;
    const users: User[] = userData;
    
    projects.forEach(p => {
        const owner = users.find(u => u.id === p.owner) as User
        const sharedWith = users.filter(u => (<number[]> p.sharedWith).includes(u.id))
        const modifiedAt = DateTime.fromISO(p.modifiedAt).toLocaleString(DateTime.DATETIME_MED)

        p.owner = owner
        p.sharedWith = sharedWith
        p.modifiedAt = modifiedAt
    })

    // Set Breadcrumbs
    const breadcrumbs = `
        <div class="breadcrumbs__list">
            <div class="breadcrumbs__item">
                Projects
            </div>

            <div class="breadcrumbs__sep">
                &#10095;
            </div>

            <div class="breadcrumbs__item breadcrumbs__item--current">
                Overview
            </div>
        </div>
    `
    $('#breadcrumbs').html(breadcrumbs)
    showSpinner()

    setTimeout(() => {
        const template = Handlebars.compile($('#project-list-tmpl').html())
        const context = {projects}
        $('#main').html(template(context))
    }, 1000)
}

/**
 * 
 * @param req 
 */
export const getOneProject = (req: any) => {
    const projects: Project[] = projectData;
    const projectId = req.params.id
    const context = projectId ? { project: projects.find(p => p.id === +projectId) } : {}

    // Set Breadcrumbs
    const breadcrumbs = `
        <div class="breadcrumbs__list">
            <div class="breadcrumbs__item--link">
                <a href="/#/projects">Projects</a>
            </div>

            <div class="breadcrumbs__sep">
                &#10095;
            </div>

            <div class="breadcrumbs__item breadcrumbs__item--current">
                ${projectId}
            </div>
        </div>
    `
    $('#breadcrumbs').html(breadcrumbs)
    showSpinner()

    setTimeout(() => {
        const template = Handlebars.compile($('#project-details-tmpl').html())
        $('#main').html(template(context))
    }, 1000)
}

/**
 * 
 * @param req 
 */
export const renderProjectForm = (req: any) => {
    const projects: Project[] = projectData;
    const projectId = req.params.id
    const context = projectId ? { project: projects.find(p => p.id === +projectId) } : {}

    let breadcrumbs = '';
    if (projectId) {
        // Set Breadcrumbs
        breadcrumbs = `
            <div class="breadcrumbs__list">
                <div class="breadcrumbs__item--link">
                    <a href="/#/projects">Projects</a>
                </div>

                <div class="breadcrumbs__sep">
                    &#10095;
                </div>

                <div class="breadcrumbs__item breadcrumbs__item--link">
                    <a href="/#/project?id=${projectId}">${projectId}</a>
                </div>

                <div class="breadcrumbs__sep">
                    &#10095;
                </div>

                <div class="breadcrumbs__item breadcrumbs__item--current">
                    Edit
                </div>
            </div>
        `
    }
    else {
        breadcrumbs = `
            <div class="breadcrumbs__list">
                <div class="breadcrumbs__item--link">
                    <a href="/#/projects">Projects</a>
                </div>

                <div class="breadcrumbs__sep">
                    &#10095;
                </div>

                <div class="breadcrumbs__item breadcrumbs__item--current">
                    New
                </div>
            </div>
        `
    }
    $('#breadcrumbs').html(breadcrumbs)
    showSpinner()

    setTimeout(() => {
        const template = Handlebars.compile($('#project-form-tmpl').html())
        $('#main').html(template(context))
    }, 1000)
}

/**
 * 
 * @param req
 */
export const createOrUpdateProject = (req: any) => {
    const projectId = req.params.id || Math.round(Math.random() * 1000)
    window.location.href = `/#/job-description/upload?projectId=${projectId}`
}

