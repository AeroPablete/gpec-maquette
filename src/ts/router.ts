
import * as Handlebars from 'handlebars';
import { createOrUpdateProject, getAllProjects, getOneProject, renderProjectForm } from './controllers/projects.controller';
import { 
    displayCandidateFilters,
    displayJobDescription, 
    uploadJobDescription } from './controllers/job-description.controller';

import {
    showFaqPage} from './controllers/static.controller';

const app: Sammy.Application = $.sammy()

/**
 * Main Page Redirection
 */
app.get('/', () => {
    window.location.href = '/#/login'
})

/**
 * Login::GET
 */
app.get('/login', (_: any) => {
    const template = Handlebars.compile($('#login-tmpl').html())
    const context = {}

    // Set Breadcrumbs
    const breadcrumbs = `
        <div class="breadcrumbs__list">
            <div class="breadcrumbs__item--current">
                Login
            </div>
        </div>
    `
    $('#breadcrumbs').html(breadcrumbs)
    $('#main').html(template(context))
})

/**
 * Login::POST
 */
app.post('/login', (_: any) => {
    window.location.href = '/#/projects'
})

/**
 * Project Endpoints
 */
app.get('/projects', getAllProjects)            // Liste de projets
app.get('/project', getOneProject)              // Detail d'un projet
app.get('/project/new', renderProjectForm)      // Creation d'un nouveau projet
app.get('/project/edit', renderProjectForm)     // Modification d'un projet
app.post('/project', createOrUpdateProject)

/**
 * Job Description Endpoints
 */
app.get('/job-description/upload', uploadJobDescription)
app.get('/job-description/display', displayJobDescription)

/**
 * Candidate Filters
 */
app.get('/candidate-filters', displayCandidateFilters)

/**
 * Static Pages
 */
app.get('/faq', showFaqPage)

/**
 * 
 */
export const startApp = () => {
    app.run()
}